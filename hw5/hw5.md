HW#5 Self Evaluation
COMP 505
Date: 2/24/2015
Student Name: Ryan O'Neal

## What I learned:
I was sick the day of the javascript lecture.  But I am learning about how javascript has direct access to all
 elements on the webpage to change them when needed.  

## How I improved by web development skills outside of class:
Reading the Learning PHP textbook and reading other articles online like www.php.net and www.khanacademy.org and 
http://www.w3.org/

## What if anything would I like clarification on:

I need more clarification on the signatures of functions, like function function_name([parameter [, ...]])
{
  // Statements
}

This confuses me because i'm not sure where I can find the commands.  I have check the 
websites like php.net but they don't
 always have all the commands I need or maybe i'm looking for the wrong ones
 
 
## For this homework I believe I should get:

Based on the syllabus I feel I should get a grade of: A-F
A