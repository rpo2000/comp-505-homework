HW#2 Self Evaluation
COMP 505
Date: 1/27/2015
Student Name: Ryan O'Neal

## What I learned:
How to explode test by using arrays to gather data from posts.  I learned about modulus operators 
and how they are used to determine if there is a remainder after a division and how variables can 
be tested to see if they have a remainder or not, and we use that data to set values of arrays.

## How I improved by web development skills outside of class:
Reading the Learning PHP textbook and reading other articles online like www.php.net and www.khanacademy.org and 
http://www.w3.org/

## What if anything would I like clarification on:
I need help with syntax and learning what operators are doing in various lines of code.

## For this homework I believe I should get:
Based on the syllabus I feel I should get a grade of: A-F
C