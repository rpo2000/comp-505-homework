# Self Evaluation
COMP 505
Date: 1/26/2015
Student Name: Ryan O'Neal

## What I learned:
   Some things I learned are about how Internet connections are made using different devices and applications.  I learned how PHP is a great language
   to use because it integrates HTLM so well.  I learned about Locally vs. remotely working on web pages.  
   I learned about using an Integrated Development Environment over a text editor. 
   I learned about WAMP and LAMP as efficient server packages.

## How I improved by web development skills outside of class:
I was able to install Linux on my home computer as a server so that I could understand LAMP more. 
 I was able to write the commands and install different packages using Linux by looking up the commands.

## What if anything would I like clarification on:

I do not understand the problems with the Git Bash and the Git GUI.  
I have tried both programs and they always give me error messages when trying to 
clone repositories and edit them.

## For this homework I believe I should get:
Based on the syllabus I feel I should get a grade of: C or C+ because I couldn't
 get the GIT to work at all at home, and only in class.
 


