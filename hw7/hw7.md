HW#7 Self Evaluation
COMP 505
Date: 3/10/2015
Student Name: Ryan O'Neal

## What I learned:
I am picking up on MySql but very slowly as this is the first time I have ever encountered this type or programming. 
 The directions in the book are concise but I still get syntax errors.

## How I improved by web development skills outside of class:
Reading the Learning PHP textbook and reading other articles online like www.php.net and www.khanacademy.org and 
http://www.w3.org/

## What if anything would I like clarification on:


User MySQLi to create a new connection to your database:  Still cant get teh code to work for connect.php


Create a new table called cities (use columns that make sense... some example data: Nashua, NH, 03063)


Check in the create table SQL as hw7.sql  I do not know how to use "PHPadmin" as the instruction in the text book are 
insufficient.

Make the form insert new records into the database:   I cannot get the database to communicate with the PHP file, 
as it says user access denied 
for that use even after typing in the proper commands to set the username and password. 
Keeps repeating syntax error, please check your manual


## For this homework I believe I should get:
Based on the syllabus I feel I should get a grade of: A-F
C