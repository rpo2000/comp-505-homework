HW#3 Self Evaluation
COMP 505
Date: 2/10/2015
Student Name: Ryan O'Neal

## What I learned:
I learned how to echo a function in PHP.  Learned how to explode an array.   

## How I improved by web development skills outside of class:
Reading the Learning PHP textbook and reading other articles online like www.php.net and www.khanacademy.org and 
http://www.w3.org/

## What if anything would I like clarification on:
Not sure about where to get all the function calls.  Or the names in the code.
I am confused on how to put together the seperate code in order to perform a function call

## For this homework I believe I should get:
Based on the syllabus I feel I should get a grade of: A-F
C